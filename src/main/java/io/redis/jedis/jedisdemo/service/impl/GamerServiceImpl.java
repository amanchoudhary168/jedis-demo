package io.redis.jedis.jedisdemo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.redis.jedis.jedisdemo.entites.Gamer;
import io.redis.jedis.jedisdemo.repositories.GammerRepo;
import io.redis.jedis.jedisdemo.service.GamerService;


@Service
public class GamerServiceImpl implements GamerService{
	
	
	@Autowired
	GammerRepo repository;
	
	@Autowired
	ObjectMapper mapper;
	
	
	public static Logger LOG = LoggerFactory.getLogger(GamerService.class);

	@Override
	public String saveGammerDetails(Gamer gammer){
		String key = null;
		try {
			String gamerDataAsString = mapper.writeValueAsString(gammer);
			key = gammer.getGameId().concat(":").concat(String.valueOf(gammer.getGamerId()));
			repository.storeGammerAsString(key, gamerDataAsString);
			
		}catch(Exception ex) {
			LOG.error("Exception Occured while saving Gammer. Details "
					+ "GammerId ={},GameId={} ,Exception= {} ",gammer.getGamerId(),gammer.getGameId(),ex);
			throw new RuntimeException("Gammer Creation failed due to Internal Exception");
		}
		LOG.info("Saved Gammer Info Successfuly. key={}",key);
		return key;
	}

	@Override
	public Gamer retriveGammerDetails(String key) {
		Gamer gamerDetails = null;
		try {
			String fetchedDataFromRepo = repository.retriveGammerAsString(key);
		    gamerDetails = mapper.readValue(fetchedDataFromRepo, Gamer.class);
			
			
		}catch(Exception ex) {
			LOG.error("Exception Occured while Fetching GamerDetails "
					+ "key={},Exception ={}",key,ex);
			throw new RuntimeException("Gammer Creation failed due to Internal Exception");
		}
		LOG.info(" Details Gamer = {}",gamerDetails);
		return gamerDetails;
	}

}
