package io.redis.jedis.jedisdemo.services;

import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.redis.jedis.jedisdemo.properties.RedisProperties;

@Service
public class ConfigurationDetailsService {
	
	public static Logger LOG = LoggerFactory.getLogger(ConfigurationDetailsService.class);
	
	@Autowired
	RedisProperties redisProperties;
	
	public LinkedHashMap<String,Object> getRedisConfigDetails(){
		
		LinkedHashMap<String,Object> redisConfigDetails = new LinkedHashMap<String,Object>();

		redisConfigDetails.put("Host",redisProperties.getHostName());
		redisConfigDetails.put("Port",redisProperties.getPort());
		redisConfigDetails.put("Pool-Size",redisProperties.getPoolSize());
		redisConfigDetails.put("Max-Idle",redisProperties.getMaxIdle());
		redisConfigDetails.put("Min-Idle",redisProperties.getMinIdle());
		LOG.debug("Redis Config Details = {}",redisConfigDetails);
		return redisConfigDetails;
		
	}

}
