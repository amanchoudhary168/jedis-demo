package io.redis.jedis.jedisdemo.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class GammerRepo {

	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	
	
	
	public void storeGammerAsString(String gammerId,String gammer) {
		redisTemplate.opsForValue().set(gammerId,gammer);
		
	}
	
	
	public String retriveGammerAsString(String gammerId) {
		return (String) redisTemplate.opsForValue().get(gammerId);
		
	}
	
	
}
