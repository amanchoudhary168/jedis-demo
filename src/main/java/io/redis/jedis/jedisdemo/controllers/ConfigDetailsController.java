package io.redis.jedis.jedisdemo.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.redis.jedis.jedisdemo.properties.RedisProperties;
import io.redis.jedis.jedisdemo.services.ConfigurationDetailsService;

@RestController()
@RequestMapping(path = "config-details/*")
public class ConfigDetailsController {
	
	@Autowired
	ConfigurationDetailsService configurationDetailsService;
	
	@GetMapping(path="/redis")
	public HashMap<String,Object> getRedisConfigDetails(){
		HashMap<String,Object> redisConfigDetails = configurationDetailsService.getRedisConfigDetails();
		return redisConfigDetails;
	}
}
