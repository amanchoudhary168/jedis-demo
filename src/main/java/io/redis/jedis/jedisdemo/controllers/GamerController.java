package io.redis.jedis.jedisdemo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.redis.jedis.jedisdemo.entites.Gamer;
import io.redis.jedis.jedisdemo.service.GamerService;

@RestController
@RequestMapping("/gamer/*")
public class GamerController {
	
	public static Logger LOG = LoggerFactory.getLogger(GamerController.class);
	
	@Autowired
	GamerService gamerService;
	
	@GetMapping(path = "/{key}")
	public Gamer getGamer(@PathVariable("key") String key) {
		LOG.info("Going to Fetch Gamer Details ,key={}",key);
		Gamer gamerInfo = gamerService.retriveGammerDetails(key);
		return gamerInfo;
		
	}
	
	
	@PostMapping(path="/create")
	public String saveGamer(@RequestBody Gamer gamer) {
		String key = gamerService.saveGammerDetails(gamer);
		return key;
	}
	

}
