package io.redis.jedis.jedisdemo.configurations;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration.JedisPoolingClientConfigurationBuilder;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import io.redis.jedis.jedisdemo.properties.RedisProperties;

@Configuration
public class JedisCommonConfiguration {
	
	@Autowired
	RedisProperties redisProperties;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		PropertySourcesPlaceholderConfigurer propertyPlaceHolder = new PropertySourcesPlaceholderConfigurer();
		Resource [] resource = new ClassPathResource[]
				{new ClassPathResource("jedis.properties")};
		propertyPlaceHolder.setLocations(resource);
		propertyPlaceHolder.setIgnoreUnresolvablePlaceholders(true);
		return propertyPlaceHolder;
	}
	
	
	
	@Bean
	public  JedisClientConfiguration jedisClientConfiguration() {
		JedisClientConfiguration.JedisPoolingClientConfigurationBuilder jedisPoolingClientConfigurationBuilder=
				(JedisPoolingClientConfigurationBuilder) JedisClientConfiguration.builder();
		GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
		poolConfig.setMinIdle(redisProperties.getMinIdle());
		poolConfig.setMaxIdle(redisProperties.getMaxIdle());
		poolConfig.setMaxTotal(redisProperties.getPoolSize());
		return jedisPoolingClientConfigurationBuilder.poolConfig(poolConfig).build();
		 
	}
	
	
	@Bean
	public JedisConnectionFactory jedisConnectionFactory() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
		System.out.println("Debug Config ==1 "+redisProperties.getHostName());
		redisStandaloneConfiguration.setHostName(redisProperties.getHostName());
		redisStandaloneConfiguration.setPort(redisProperties.getPort());
		redisStandaloneConfiguration.setDatabase(0);
		return new JedisConnectionFactory(redisStandaloneConfiguration,jedisClientConfiguration());
	}
	
	@Bean
	public RedisTemplate<String, Object> redisTemplate(){
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(jedisConnectionFactory());
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
		return redisTemplate;
	} 
	
	
	
}
