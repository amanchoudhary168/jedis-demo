package io.redis.jedis.jedisdemo.entites;

import java.io.Serializable;

public class Gamer implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int gamerId;
	String gamerName;
	long score;
	String gameId;
	
	
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	public int getGamerId() {
		return gamerId;
	}
	public void setGamerId(int gamerId) {
		this.gamerId = gamerId;
	}
	public String getGamerName() {
		return gamerName;
	}
	public void setGamerName(String gammerName) {
		this.gamerName = gammerName;
	}
	public long getScore() {
		return score;
	}
	public void setScore(long score) {
		this.score = score;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + gamerId;
		result = prime * result + ((gamerName == null) ? 0 : gamerName.hashCode());
		result = prime * result + (int) (score ^ (score >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gamer other = (Gamer) obj;
		if (gamerId != other.gamerId)
			return false;
		if (gamerName == null) {
			if (other.gamerName != null)
				return false;
		} else if (!gamerName.equals(other.gamerName))
			return false;
		if (score != other.score)
			return false;
		return true;
	}
	

}
